// julia_128_hex.cpp  230809
// 231226
//g++ julia_1000_hex.cpp -lglut -lGL -o jshx1000.o
//    c  ci    colors   scale    iterations  limit  color_modulo color_factor   hx1  hx2  hx3 
// jshx1000.o -.17 -.75 16 700 1000 0 16 2 0000ff 007700 001100	

#include <stdio.h>
#include "GL/glut.h"
#include "GL/gl.h"
#include <iostream>
#include <array>
using namespace std; // c++ namespace
//globals for command line input 
string hexs1, hexs2, hexs3;
int  interations_arg, limit_arg, count_mod_arg,colors_arg,colors_mod_arg;
int rsi, gsi, bsi, rmi, gmi, bmi, rei, gei, bei; 
float c1_arg, c2_arg, scale_arg,rsft, gsft, bsft,rmft, gmft, bmft,reft,geft,beft;
float count_factor;
float round4(float var)
{
    float value = (int)(var * 10000 + 0.5);
    return (float)value / 10000;
}

// two digit hex to decimal conversion
int decimalFromHex(string h) {
	int d10,d1; //
	char sixteens,ones;
	//cout<<" h "<<h<<endl;
	sixteens = h[0]; //sixteenths place
	ones = h[1];     // ones place
	d10 = int(sixteens);
	if (d10 < 97){
		d10 = (d10 - 48)*16;
	}else{
		d10 = (d10 - 87)*16;
	}
	d1 = int(ones);
	if (d1 < 97){
		d1 = (d1 - 48);
	}else{
		d1 = (d1 - 87);
	}
	// debug cout
	//char c = 'g';
	//cout<<" 16 1 * "<<sixteens<<","<<ones<<endl;
	//cout<<" 10 1 "<<d10<<","<<d1<<endl;
	//cout<<" d10+d1 "<<d10+d1<<endl;
	return d10+d1;
}

void plot2D(float x,float y, float red, float green, float blue){
    glColor3f(red, green, blue);
    glVertex2f(x, y);
}

// buildColors  * * * * * ** * * * * ** * * * * ** * * * * ** * * * * ** * * * * ** * * * * ** * * * * *
array<array<float,3>,1000> buildColors(  array<array<float,3>,3> colors, int numberOfColors){
  array<array<float,3>,1000>ColorsA;//Colors Array
    int n;
    float colors_sm, colors_me;
    int m  = 0; // m is the counter
    float sr,sg,sb,mr,mg,mb,er,eg,eb,ir,ig,ib,ir2,ig2,ib2;//incremant red, green , blue
      //colors_sm = 400;  // colors start middle
   //colors_me = 600; // colors middle end
    colors_sm = (float) numberOfColors/2;
    colors_me = (float) numberOfColors/2;
 
    cout<<"Number of colors: "<<numberOfColors<<endl;
      cout<<"colors_sm : "<<colors_sm <<endl;
     cout<<"colors_me : "<<colors_me <<endl;
     // output colors
    for (n =0;n < 3 ; n++){
      cout<<"colors n "<<n<<" | " << colors[n][0]<<" "<<colors[n][1]<<" "<<colors[n][2]<<endl;
    }
    // set increments for colors
    cout<<" set sr sg sb . . . "<<endl;
    sr = colors[m][0];sg = colors[m][1];sb = colors[m][2];  // start r g b
	cout<<" set mr mg mb . . . "<<endl;
    mr = colors[m+1][0];mg = colors[m+1][1]; mb= colors[m+1][2];  // middle r g b
    cout<<" set er eg eb . . . "<<endl;
    er = colors[m+2][0];eg = colors[m+2][1];eb = colors[m+2][2];  // end rgb
     cout<<" set ir ir2 "<<endl;
    ir = ((mr-sr)/(colors_sm))*2;ig = ((mg-sg)/(colors_sm))*2; ib =((mb-sb)/(colors_sm))*2;
        ir = (mr-sr)/(colors_sm);ig = (mg-sg)/(colors_sm); ib =(mb-sb)/(colors_sm);
   ir2 = ((er-mr)/(colors_me))*2;ig2 = ((eg-mg)/(colors_me))*2; ib2 =((eb-mb)/(colors_me))*2;
	//   ir2 = ((er-mr)/(colors_me));ig2 = ((eg-mg)/(colors_me)); ib2 =((eb-mb)/(colors_me));
  //  if(ir < 0.0)ir = ir*-1.0; if(ig < 0.0)ig = ig * -1.0; if(ib < 0.0)ib = ib * -1.0;
    //ir = 0.001; ig = 0.001; ib = 0.001;
    cout<<"increments " << ir <<" "<<ig<<" "<<ib<<endl;
    m = 0;// set first color 
    ColorsA[m][0] = colors[m][0];
    ColorsA[m][1] = colors[m][1];
    ColorsA[m][2] = colors[m][2];
     cout<<"increments2 " << ir2 <<" "<<ig2<<" "<<ib2<<endl;
  m = 1; // increment counter m
    while(m < 1000){
            ColorsA[m][0] = ColorsA[m-1][0] + ir;
              if(ColorsA[m][0] < 0.0)ColorsA[m][0] = 0.0;// just in case
              if(ColorsA[m][0] > 1.0)ColorsA[m][0] = 1.0;
            ColorsA[m][1] = ColorsA[m-1][1] + ig;
              if(ColorsA[m][1] < 0.0)ColorsA[m][1] = 0.0;
              if(ColorsA[m][1] > 1.0)ColorsA[m][1] = 1.0;
            ColorsA[m][2] = ColorsA[m-1][2] + ib;
              if(ColorsA[m][2] < 0.0)ColorsA[m][2] = 0.0;
              if(ColorsA[m][2] > 1.0)ColorsA[m][2] = 1.0;
              m = m + 1;
            if(m > (667)){
			    ir = ir2; ig = ig2; ib = ib2;
			  }
                  //cout<<" m = "<<m<<endl;
	}
    
for (n =0;n < 128 ; n++){
   // cout<<"n="<<n<<" " << ColorsA[n][0]<<" "<<ColorsA[n][1]<<" "<<ColorsA[n][2]<<endl;
    }
 // cout<<" return \n";
    return ColorsA;
}// end buildcolors * * * * * ** * * * * ** * * * * ** * * * * ** * * * * ** * * * * ** * * * * ** * * * * ** * * * * ** * * * * *

void drawPoints(){
  //float color256[256][3] = {};
  // these are the starting colors
    //from global rsi rsf
	//start color
	rsft = (float)rsi/255.0;
	gsft = (float)gsi/255.0;
	bsft = (float)bsi/255.0;
	//middle color
	rmft = (float)rmi/255.0;
	gmft = (float)gmi/255.0;
	bmft = (float)bmi/255.0;
	//end color
	reft = (float)rei/255.0;
	geft = (float)gei/255.0;
	beft = (float)bei/255.0;
	int total_colors = colors_arg;
  array<array<float,3>,3> colors = {{
	  {rsft, gsft, bsft},
	    {rmft, gmft, bmft},
	  {reft, geft, beft}
    }};
    //colorsA colors array
  array<array<float,3>,1000>colorsA = buildColors(colors,total_colors);
    //array<array<float,3>,128>colorsA = buildColors(colors);
    int setcolor = 0;
    int iterations = interations_arg; //calulations iterations number
    int limit = limit_arg;// plot limit
    float h,k;
   int count,maxcount;
    float z,x,y,x1,y1,x2,y2,jd,id;
    // .34567 .5
    float c1 = 0.3456;
    float c2 = 0.5321;
    c1 = c1_arg; c2 = c2_arg;
    float red = 1.0,green = 0 ,blue = 0.0;
    glClearColor(1.0,1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(1.0, 1.0, 1.0);
    //glOrtho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);
    glOrtho(-500.0, 550.0, -500.0, 500.0, -500.0, 500.0);
    glPointSize(1.0);// set pixel size
    glBegin(GL_POINTS);// points
    // ************************************************************
    maxcount = 0;
    	float scale_count,count_mod_float,sc;
    //cout<<" plot \n ";
    float scale = scale_arg/2.0;
    //cout<<" scale limit "<<scale<<" "<<" "<<iterations<<" "<<limit<<endl;
      for (k = -500.0 ; k <= 500.0 ; k = k + 1 ){
			y1 = k / scale;
				for (h = -500.0 ; h <= 500.0; h = h + 1){
					x1 =  h / scale;
					//x = x1;
					//y = y1;
					x = x1;
					y = y1;
					z = 0.0;
					count = 0;
					do {
						x2 = (x * x) - (y * y) + c1;
						y = (2 * x * y) + c2;
						x = x2;
						z = (x * x) + (y * y);
						count++;
					}while ((count <= iterations) & (z < 4.0));
			//if(count > 128) count = 128;
            if(count > maxcount)maxcount = count;
            //cout<<"count "<<count<<endl;
				if (count >= limit) {
					//cout<<count<<" count count mod arg "<<count_mod_arg<<endl;
					scale_count = (float)(count * count_factor);
					setcolor = (int)(scale_count)%count_mod_arg ;
				//	cout<<" count scale_count set color "<<count<<" "<<scale_count<<" "<<setcolor<<endl;
				//	if (setcolor > colors_arg) setcolor = colors_arg;
      				red = colorsA[setcolor][0];
					green= colorsA[setcolor][1];
      				blue = colorsA[setcolor][2];
      				plot2D(h,k,red,green,blue);

				}
	    	}//end for i
        }//end for j
        cout<<" max count "<<maxcount<<endl;
        
// print gradiant
	 z = 0;
	 int a = 0;
	
				for (k = 0.0 ; k < 1000.0; k = k + 1){
					for (a = 0; a < 40; a++){
						red = colorsA[z][0];
						green= colorsA[z][1];
						blue = colorsA[z][2];
						plot2D(510+a,k-500,red,green,blue);
					}
					z = z + 1;
				}
	
    // ************************************************************
    glEnd();//end points
    glutSwapBuffers();// this draws the points
}

int main(int argc, char **argv)
{

    cout<<" c " <<argv[1]<<"\n";
    cout<<" ci "<< argv[2]<<"\n";
    cout<<" colors "<<argv[3]<<"\n";
    cout<<" scale "<< argv[4]<<"\n";
    cout<<" iterations "<< argv[5]<<"\n"; // iteration
	cout<<" limit "<< argv[6]<<"\n"; // count limit
    cout<<" color modulo "<< argv[7]<<"\n";
    cout<<" color factor "<< argv[8]<<"\n";
    cout<<" hx1 "<<argv[9]<<"\n";
	cout<<" hx2 "<<argv[10]<<"\n"; 
	cout<<" hx3 "<<argv[11]<<"\n";
	cout<<" c ci colors scale iterations limit color_modulo color_factor hex1 hex2 hex3  "<<endl;
 
    c1_arg = stof(argv[1]);
    c2_arg = stof(argv[2]);
    colors_arg = stoi(argv[3]);
    scale_arg = stof(argv[4]);
    interations_arg = stoi(argv[5]);
	limit_arg = stoi(argv[6]);// plot limit
    count_mod_arg = stoi(argv[7]); // count mod
    count_factor = stof(argv[8]);
    hexs1 = argv[9]; hexs2 = argv[10]; hexs3 = argv[11];
	rsi = decimalFromHex(hexs1.substr(0,2));
	gsi = decimalFromHex(hexs1.substr(2,2));
	bsi = decimalFromHex(hexs1.substr(4,2));
    rmi = decimalFromHex(hexs2.substr(0,2));
	gmi = decimalFromHex(hexs2.substr(2,2));
	bmi = decimalFromHex(hexs2.substr(4,2));
	rei = decimalFromHex(hexs3.substr(0,2));
	gei = decimalFromHex(hexs3.substr(2,2));
	bei = decimalFromHex(hexs3.substr(4,2));

   // red = decimalFromHex(hexs3.substr(0,2));
//	green = decimalFromHex(hexs3.substr(2,2));
//	blue = decimalFromHex(hexs3.substr(4,2));
	
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE);
    glutInitWindowSize(1050, 1000);
    glutInitWindowPosition(500, 10);
    glutCreateWindow("Julia Set  f(c) = (c+ci)^2");
    glutDisplayFunc(drawPoints);
    glutMainLoop();
    return 0;
}
