//  g++ Juila_2_256_hex_0.cpp -lglut -lGL -o nm3-cc.o
// cw coleman 240309
// ./nm3-cc.o 32 400 1000 63 63 63  0 255 127 127 127 255 1
#include <stdio.h>
#include "GL/glut.h"
#include "GL/gl.h"
#include <iostream>
#include <array>
using namespace std; // c++ namespace
//globals for command line input
string hexs1, hexs2, hexs3, hexs4;
int  interations_arg, limit_arg, count_mod_arg,colors_arg,colors_mod_arg;
int rsi, gsi, bsi, rmi, gmi, bmi, rei, gei, bei; 
float c1_arg, c2_arg, scale_arg,rsft, gsft, bsft,rmft, gmft, bmft,reft,geft,beft;
float count_factor;
float round4(float var)
{
    float value = (int)(var * 10000 + 0.5);
    return (float)value / 10000;
}

array<array<int,3>,4>colorInputs;//integer color inputs from args


// two digit hex to decimal conversion
int decimalFromHex(string h) {  // convert hexstring to integer decimal values
		int d10,d1; 
	char sixteens,ones;
	sixteens = h[0]; //sixteenths place
	ones = h[1];     // ones place
	d10 = int(sixteens);
	if (d10 < 97){
		d10 = (d10 - 48)*16;
	}else{
		d10 = (d10 - 87)*16;
	}
	d1 = int(ones);
	if (d1 < 97){
		d1 = (d1 - 48);
	}else{
		d1 = (d1 - 87);
	}
	return d10+d1;
}


// plot a single point
void plot2D(float x,float y, float red, float green, float blue){
    glColor3f(red, green, blue);
    glVertex2f(x, y);
}
//////
// buildColors * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

array<array<float,3>,256> buildColors(  int numberOfColors){
  array<array<float,3>,256>Colors256;
  array<array<float,3>,4>startColor;
    int i,j,n;
    float colors_se;
    int m  = 0; // m is the counter
    float sr,sg,sb,er,eg,eb,ir,ig,ib;//incremant red, green , blue
    
    for (i = 0 ; i < 4; i++){
		//cout<<"rgb ";
			for (j = 0; j < 3;j++){
					//cout<< colorInputs[i][j]<< " ";
					startColor[i][j] = float(colorInputs[i][j]/255.0);
			}
	}
/*	cout<<"\n";
	    for (i = 0 ; i < 4; i++){
			cout<<"rgb float ";
			for (j = 0; j < 3;j++){
					cout <<startColor[i][j]<<" ";					
			}
	}
*/	
    cout<<"\n buildColors numberOfColors: "<<numberOfColors<<endl;

int color0 = 0; //second color
float currentR0,currentG0,currentB0,currentR1,currentG1,currentB1;
  m = 0; // set m to zero
	while(m < 256){
		if(m ==0){
			ir = (startColor[color0+1][0] - startColor[color0][0])/numberOfColors*3;
			ig = (startColor[color0+1][1] - startColor[color0][1])/numberOfColors*3;
			ib =  (startColor[color0+1][2] - startColor[color0][2])/numberOfColors*3;
			//cout<<"increments m "<<m << ir <<" "<<ig<<" "<<ib<<endl;
			//set start color
			Colors256[m][0] = startColor[color0][0];
			Colors256[m][1] = startColor[color0][1];
			Colors256[m][2] = startColor[m][2];
			currentR0 =  startColor[m][0] ;currentG0 = startColor[m][1]  ; currentB0 = startColor[m][2] ;
			currentR1 =  startColor[m+1][0] ;currentG1 = startColor[m+1][1]  ; currentB1 = startColor[m+1][2] ;
		m = m + 1; // increment counter m	by 1
		}//end if
		
		if(m ==85){
			color0  = 1;
			ir = (startColor[color0+1][0] - startColor[color0][0])/numberOfColors*3;
			ig = (startColor[color0+1][1] - startColor[color0][1])/numberOfColors*3;
			ib =  (startColor[color0+1][2] - startColor[color0][2])/numberOfColors*3;
			//cout<<"increments m "<<m << ir <<" "<<ig<<" "<<ib<<endl;
			//set start color
			Colors256[m][0] = startColor[color0][0];
			Colors256[m][1] = startColor[color0][1];
			Colors256[m][2] = startColor[color0][2];
			currentR0 =  startColor[m][0] ;currentG0 = startColor[m][1]  ; currentB0 = startColor[m][2] ;
			currentR1 =  startColor[m+1][0] ;currentG1 = startColor[m+1][1]  ; currentB1 = startColor[m+1][2] ;
		 m = m + 1; // increment counter m	by 1
		}//end if
		
		if(m ==170){
			color0  = 2;
			ir = (startColor[color0+1][0] - startColor[color0][0])/numberOfColors*3;
			ig = (startColor[color0+1][1] - startColor[color0][1])/numberOfColors*3;
			ib =  (startColor[color0+1][2] - startColor[color0][2])/numberOfColors*3;
			//cout<<"increments m "<<m << ir <<" "<<ig<<" "<<ib<<endl;
			//set start color
			Colors256[m][0] = startColor[color0][0];
			Colors256[m][1] = startColor[color0][1];
			Colors256[m][2] = startColor[color0][2];
			currentR0 =  startColor[m][0] ;currentG0 = startColor[m][1]  ; currentB0 = startColor[m][2] ;
			currentR1 =  startColor[m+1][0] ;currentG1 = startColor[m+1][1]  ; currentB1 = startColor[m+1][2] ;
		 m = m + 1; // increment counter m	by 1
		}//end if
		
            Colors256[m][0] = Colors256[m-1][0] + ir;
             if(Colors256[m][0] < 0.0){Colors256[m][0] = currentR0 ;   }
             if(Colors256[m][0] > 1.0){Colors256[m][0] = currentR1 ;     }
            Colors256[m][1] = Colors256[m-1][1] + ig;
              if(Colors256[m][1] < 0.0){Colors256[m][1] = currentG0 ;    }
              if(Colors256[m][1] > 1.0){Colors256[m][1] = currentG1 ;  }
           Colors256[m][2] = Colors256[m-1][2] + ib;
             if(Colors256[m][2] < 0.0){Colors256[m][2] = currentB0 ;      }
             if(Colors256[m][2] > 1.0){Colors256[m][2] = currentB1 ;     }
         
              m = m + 1;
              
                  //cout<<" m = "<<m<<endl;
	}
	
//for (n =0;n < 256 ; n++){
//   cout<<"n="<<n<<" " << Colors256[n][0]<<" "<<Colors256[n][1]<<" "<<Colors256[n][2]<<endl;
 //   }
 // cout<<" return \n";
    return Colors256;
}// end buildcolors


//////

void drawPoints(){
//float color256[256][3] = {};
  // these are the starting colors
    //from global rsi rsf
	//start color
	rsft = (float)rsi/255.0;
	gsft = (float)gsi/255.0;
	bsft = (float)bsi/255.0;
	//end color
	reft = (float)rei/255.0;
	geft = (float)gei/255.0;
	beft = (float)bei/255.0;
	int total_colors = colors_arg;
  array<array<float,3>,2>colors = {{
		{rsft, gsft, bsft},
		{reft, geft, beft}
	  }};

   array<array<float,3>,256>colors256 = buildColors(total_colors);
 
    int setcolor = 0;
    int iterations = interations_arg; //calulations iterations number
    int limit = limit_arg;// plot limit
    float h,k;
   int count,maxcount;
    float z,x,y,x1,y1,x2,y2,jd,id;
    // .34567 .5
    float c1,c2;
    c1 = c1_arg; c2 = c2_arg;
    float red = 1.0,green = 0 ,blue = 0.0;
    glClearColor(1.0,1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(1.0, 1.0, 1.0);
    //glOrtho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);
    glOrtho(-500.0, 550.0, -500.0, 500.0, -500.0, 500.0);
    glPointSize(1.0);// set pixel size
    glBegin(GL_POINTS);// points
    // ************************************************************
    maxcount = 0;
    	float scale_count,count_mod_float,sc;
    //cout<<" plot \n ";
    float scale = scale_arg/2.0;
    //cout<<" scale limit "<<scale<<" "<<" "<<iterations<<" "<<limit<<endl;
      for (k = -500.0 ; k <= 500.0 ; k = k + 1 ){
			y1 = k / scale;
				for (h = -500.0 ; h <= 500.0; h = h + 1){
					x1 =  h / scale;
					//x = x1;
					//y = y1;
					x = x1;
					y = y1;
					z = 0.0;
					count = 0;
					do {
						x2 = (x * x) - (y * y) + c1;
						y = (2 * x * y) + c2;
						x = x2;
						z = (x * x) + (y * y);
						count++;
					}while ((count <= iterations) & (z < 4.0));
            if(count > maxcount)maxcount = count;
           //cout<<count<<endl;
				if (count >= limit) {
					//cout<<count<<" count count mod arg "<<count_mod_arg<<endl;
					scale_count = (float)(count * count_factor);
					setcolor = (int)(scale_count)%count_mod_arg ;
					//setcolor = (int)(count)%colors_arg ;
							
				//	cout<<" count scale_count set color "<<count<<" "<<scale_count<<" "<<setcolor<<endl;
				//	if (setcolor > colors_arg) setcolor = colors_arg;
      				red = colors256[setcolor][0];
					green= colors256[setcolor][1];
      				blue = colors256[setcolor][2];
      				plot2D(h,k,red,green,blue);

				}
	    	}//end for i
        }//end for j
        cout<<" max count "<<maxcount<<endl;
      //  printf("max count %d \n",maxcount);
	// print gradiant
	int gc = 0, a = 0;
	
				for (k = 0.0 ; k < 256.0; k = k + 1){
					for (a = 0; a < 40; a++){
						red = colors256[gc][0];
						green= colors256[gc][1];
						blue = colors256[gc][2];
						plot2D(510+a,k-100,red,green,blue);
					}
					gc++;
				}
	
      // only change code above this line
    // ************************************************************
    glEnd();//end points
    glutSwapBuffers();// this draws the points
}

int main(int argc, char **argv)
{
  // colors scale limit count_multiplier hx1 hx2 hx3 hx4
  cout<<" c " <<argv[1]<<" ";
    cout<<" ci "<< argv[2]<<"\n";
    cout<<" colors "<<argv[3]<<" ";
    cout<<" scale "<< argv[4]<<" ";
    cout<<" iterations "<< argv[5]<<" "; // iteration
	cout<<" limit "<< argv[6]<<" "; // count limit
    cout<<" color modulo "<< argv[7]<<" ";
    cout<<" color mod factor "<< argv[8]<<"\n";
    cout<<" "<<argv[9]<<" ";
	cout<<" "<<argv[10]<<" "; 
	cout<<" "<<argv[11]<<" ";
	cout<<" "<<argv[12]<<" "; 
     c1_arg = stof(argv[1]);
    c2_arg = stof(argv[2]);
    colors_arg = stoi(argv[3]);
    scale_arg = stof(argv[4]);
    interations_arg = stoi(argv[5]);
	limit_arg = stoi(argv[6]);// plot limit
    count_mod_arg = stoi(argv[7]); // count mod
    count_factor = stof(argv[8]);

	 hexs1 = argv[9]; hexs2 = argv[10]; hexs3= argv[11];hexs4 = argv[12];
	 // rgb 1
	 colorInputs[0][0] = decimalFromHex(hexs1.substr(0,2));// red 1
	 	 colorInputs[0][1] = decimalFromHex(hexs1.substr(2,2)); //green 1
	 	 	 colorInputs[0][2] = decimalFromHex(hexs1.substr(4,2)); //blue 1
	 	 // rgb 2
	 colorInputs[1][0] = decimalFromHex(hexs2.substr(0,2));// red 2
	 	 colorInputs[1][1] = decimalFromHex(hexs2.substr(2,2)); //green 2
	 	 	 colorInputs[1][2] = decimalFromHex(hexs2.substr(4,2)); //blue 2
	 	 	  	 // rgb 3
	 colorInputs[2][0] = decimalFromHex(hexs3.substr(0,2));// red 3
	 	 colorInputs[2][1] = decimalFromHex(hexs3.substr(2,2)); //green 3
	 	 	 colorInputs[2][2] = decimalFromHex(hexs3.substr(4,2)); //blue 3
	 	 	 	 	  	  	 // rgb 4
	 colorInputs[3][0] = decimalFromHex(hexs4.substr(0,2));// red 4
	 	 colorInputs[3][1] = decimalFromHex(hexs4.substr(2,2)); //green 4
	 	 	 colorInputs[3][2] = decimalFromHex(hexs4.substr(4,2)); //blue 4
	 	 	 	 	 
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE);
    glutInitWindowSize(1050, 1000);
    glutInitWindowPosition(300, 100);
    glutCreateWindow("Julia Set");
    glutDisplayFunc(drawPoints);
    glutMainLoop();
    return 0;
}
